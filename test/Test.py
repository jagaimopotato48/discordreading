# Test.py
# DiscordReadingのテスト
# Test.batから呼ばれることが前提になっています

#imports
import sys
import time
import discord

#constants
# discordクライアントインスタンス
DISCORD_CLIENT = discord.Client()
# テスト用文字列
VALID_STRINGS = ["apple", "a", "a aa aaa", "あ", "あ　ああ　あああ", "aaああ",
        "ああaa", "aaああaaああ", "aa ああ　aa　ああ aa", "aa\naa","ああ\nああ",
        "＠", "ｈｔｔｐ",
        "012345678901234567890123456789012345678901234567890123456789012",
        "012345678901234567890123456789\n01234567890123456789012345678901"]
INVALID_STRINGS = ["@", "aa@", "@aa", "@here", "@speech_by_voiceroid", "http",
        "https", "https://test.com", "aahttp",
        "0123456789012345678901234567890123456789012345678901234567890123",
        "012345678901234567890123456789\n012345678901234567890123456789012",
        "<:"]
# テスト用待機時間
SLEEP_TIME = [1, 1, 2, 1, 3, 2, 2, 3, 3, 2, 2, 2, 2, 16, 17]

async def Test():
    """DiscordReadingのテストを行う
    """
    channel = DISCORD_CLIENT.get_channel(int(sys.argv[2]))
    await channel.send("テストを開始します。")
    time.sleep(2)
    await channel.send("正常系のテストを開始します。")
    time.sleep(3)
    validTestID = 0
    for valid_string in VALID_STRINGS:
        await channel.send(valid_string)
        time.sleep(SLEEP_TIME[validTestID])
        validTestID = validTestID + 1
    await channel.send("異常系のテストを開始します。")
    time.sleep(3)
    for invalid_string in INVALID_STRINGS:
        await channel.send(invalid_string)
        time.sleep(3)
    await channel.send("テストを終了します。")

@DISCORD_CLIENT.event
async def on_ready():
    """ディスコードへのログインが成功した時発火するイベント
    """
    await Test()
    await sys.exit()

# client.run()が正常に処理された時点でテストが開始される
# client.run()はそれ以降の処理の進行をブロックするのでコード最下部に記述されている
if __name__ == "__main__":
    DISCORD_CLIENT.run(sys.argv[1])