# DiscordReading.py
# エントリポイントとdiscord関連のイベント
# DiscordReading.batから呼ばれることが前提になっています。

# imports
import sys
import discord
from voiceroid2 import GetVoiceroid

# constants
# discordクライアントインスタンス
DISCORD_CLIENT = discord.Client()
# voiceroidインスタンス
VOICEROID = GetVoiceroid()
# 発言する最大文字列長
MAX_SPEECH_LENGTH = 64
# 発言不可能な文字、または文字列のリスト
BAN_STRINGS = ["@", "http", "<:"]

def PrintMessageInformation(message):
    """引数として受け取ったメッセージの情報を出力する
        Args:
            message (string): メッセージ文字列
    """
    print("===========================================")
    print(message.created_at)
    print("server  : {0}".format(message.guild))
    print("channel : {0}".format(message.channel))
    print("author  : {0}".format(message.author))
    print(message.clean_content)
    print("attachments : ")
    for attachment in message.attachments:
        print(attachment.url)
    print("===========================================")
    print("")

# ディスコードへのログインが成功した時発火するイベント
@DISCORD_CLIENT.event
async def on_ready():
    """ディスコードへのログインが成功した時発火するイベント
    """
    print("DiscordReading")

@DISCORD_CLIENT.event
async def on_message(message):
    """メッセージの書き込みがあった時発火するイベント
        Args:
            message (message): discordのメッセージ構造体
    """
    # 一応標準出力には情報を出しておく
    PrintMessageInformation(message)

    # message はメッセージに関する情報を持つ構造体なので必要なメッセージ本体だけ取り出す
    content = message.content

    # メッセージが長すぎる場合、発言しないようにチェック
    if len(content) >= MAX_SPEECH_LENGTH:
        content = "メッセージが長すぎます。"
    # 発言不可能な文字、または文字列が含まれているかのチェック
    else:
        for ban_string in BAN_STRINGS:
            if ban_string in content:
                content = "発言できない文字列が含まれています。"
    
    # チャット内容を発言。発言できなければその旨を伝える。
    VOICEROID.Say(content)

# client.run()はそれ以降の処理の進行をブロックするのでコード最下部に記述されている。
if __name__ == "__main__":
    DISCORD_CLIENT.run(sys.argv[1])